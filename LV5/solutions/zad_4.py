# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 15:22:34 2020

@author: student
"""

from sklearn import cluster
import numpy as np
import matplotlib.pyplot as plt
import cv2
    
img=cv2.imread(r'..\resources\example_grayscale.png',0)

X = img.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=2,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = img.shape

plt.figure(1)
plt.imshow(img,  cmap='gray')

plt.figure(2)
plt.imshow(img_compressed,  cmap='gray')

