fname = input('Enter the file name: ')  #e.g. www.py4inf.com/code/romeo.txt
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

domains=dict()
mails=[]
i=0
for line in fhand:
    words = line.split()
    
    for idx, word in enumerate(words):
        if(word=="From"):
            mail=words[idx+1]
            mails.append(mail)
            domain=mail[mail.find('@')+1::]
            
            if domain in domains:
                domains[domain]+=1
            else:
                domains[domain]=1

i=0
for domain in domains:
    print(domain)
    print(mails[i])
    i+=1
    if(i>=3):
        break

