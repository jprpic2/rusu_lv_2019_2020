def avg(numbers):
    return sum(numbers)/len(numbers)

fname = input('Enter the file name: ')  #e.g. www.py4inf.com/code/romeo.txt
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
spamConfidence=[]
for line in fhand:
    words = line.split()
    
    for idx, word in enumerate(words):
        if(word=="X-DSPAM-Confidence:"):
            spamConfidence.append(float(words[idx+1]))

print("Ime datoteke: ", fname)
print("Average X-DSPAM-Confidence: ",avg(spamConfidence))
