# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 19:50:00 2021

@author: Jakov-PC
"""

import numpy as np
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import plot_confusion_matrix

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T

data=non_func(100)

#Data normalization
data = (data - data.min()) / (data.max()-data.min())
X=data[:,0:2]
y=data[:,2]
solvers=['adam','lbfgs','adam','lbfgs']
activation=['relu','relu','tanh','tanh']
colors=['purple','blue','black','red']

#Data split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1,test_size=0.25)

#Meshgrid for decision boundary
h = .001
x_min, x_max = X[:, 0].min(), X[:, 0].max()
y_min, y_max = X[:, 1].min(), X[:, 1].max()
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
plt.figure()
for i in range(4):
    #activation='relu', optimizer='adam'
    regr = MLPRegressor(random_state=1,
                        activation=activation[i],
                        solver=solvers[i],
                    max_iter=1000,
                    batch_size=20,
                    alpha=0.1,
                    early_stopping=True,
                    n_iter_no_change=100,
                    hidden_layer_sizes=(4,4)).fit(X_train, y_train)
    # lossCurves.append(regr.loss_curve_)

    
    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    Z = regr.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.figure(i)
    plt.contour(xx, yy, Z, cmap='viridis')
    plt.scatter(X_test[:,0],X_test[:,1],c=regr.predict(X_test))
    plt.title('Activation: ' + str(activation[i]) + '\n' + 'Optimizer: ' + str(solvers[i]))
    if(i==3):
        plt.pause(2)
    plt.show()
    print('Activation: ' + str(activation[i]) + '\n' 
          + 'Optimizer: ' + str(solvers[i]) + '\n' 
          + 'Score: ' + str(regr.score(X_test,y_test)) + 
          '\n')

from sklearn.linear_model import LinearRegression
reg = LinearRegression().fit(X_train, y_train)
print('Logistic Regression' + '\n' +
      'Score: '+ str(reg.score(X_test,y_test)))

Z = regr.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)
plt.figure(6)
plt.contour(xx, yy, Z, cmap='viridis')
plt.scatter(X_test[:,0],X_test[:,1],c=reg.predict(X_test))
plt.title('Logistic Regression')
plt.show()



