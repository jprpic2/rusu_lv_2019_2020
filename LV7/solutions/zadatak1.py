# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 18:44:54 2021

@author: Jakov-PC
"""


import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

def generate_data(n):
	
	#prva klasa
	n1 = int(n/2)
	x1_1 = np.random.normal(0.0, 2, (n1,1));
	
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
	
	#druga klasa
	n2 = int(n - n/2)
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)
	
	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]
	
	return data

data=generate_data(1000)

X=data[:,0:2]
y=data[:,2]
lossCurves=[]
colors=['purple','blue','black','red']
layerSizes=[(10,10),(5,5,5,5),(25,25),(15,15,15,15)]
#MinMax normalization
X = (X - X.min()) / (X.max()-X.min())
#Split data into train and test
X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=1,test_size=0.25)

#Meshgrid for decision boundary
h = .001
x_min, x_max = X[:, 0].min(), X[:, 0].max()
y_min, y_max = X[:, 1].min(), X[:, 1].max()
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
plt.figure()
for i in range(4):
    #activation='relu', optimizer='adam'
    clf = MLPClassifier(random_state=1,
                    max_iter=500,
                    batch_size=20,
                    early_stopping=True,
                    n_iter_no_change=100,
                    hidden_layer_sizes=(layerSizes[i])).fit(X_train, y_train)
    lossCurves.append(clf.loss_curve_)

    
    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.figure(i)
    plt.contour(xx, yy, Z, colors=colors[i])
    plt.scatter(X_test[:,0],X_test[:,1],c=clf.predict(X_test))
    plt.title('Layer size: ' + str(layerSizes[i]))
    if(i==3):
        plt.pause(2)
    plt.show()
    
from sklearn.linear_model import LogisticRegression

logreg=LogisticRegression().fit(X_train, y_train)
Z = logreg.predict(np.c_[xx.ravel(),yy.ravel()])
Z = Z.reshape(xx.shape)
plt.figure(5)

plt.contour(xx, yy, Z, colors='r')
plt.scatter(X_test[:,0],X_test[:,1],c=logreg.predict(X_test))
plt.title('Logistic Regression')
plt.pause(2)
plt.show()
    
plt.figure(6)
for i in range(4):
    plt.plot(np.arange(len(lossCurves[i])),lossCurves[i],c=colors[i])
plt.legend(layerSizes)
plt.title('Loss function')
plt.xlabel('Iteration')
plt.ylabel('Loss')
plt.show()

