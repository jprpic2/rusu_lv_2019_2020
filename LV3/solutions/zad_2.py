# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 09:13:32 2020

@author: student
"""

import pandas as pd

mtcars = pd.read_csv(r"..\resources\mtcars.csv")

# ZADATAK 1

# cars_cyls=mtcars[['mpg','cyl']].groupby('cyl')
# cars_cyls=cars_cyls.mean()
# cars_cyls.plot.bar(rot=0)

# ZADATAK 2

# cars_weight=mtcars[['wt','cyl']]
# cars_weight.boxplot(by='cyl')

# ZADATAK 3

cars_mpg=mtcars[['mpg','am']]
print(cars_mpg)
cars_mpg.boxplot(by='am')


# ZADATAK 4

# cars_speed = mtcars[['hp','qsec','am']].groupby('am')
# cars_speed=cars_speed.mean()
# cars_speed.plot.bar(rot=0)

