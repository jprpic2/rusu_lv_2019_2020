# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas as pd


mtcars = pd.read_csv(r"..\resources\mtcars.csv")
#print(mtcars)

cars_mpg=mtcars.sort_values("mpg")
#print(cars_mpg.tail(5))  #1

cars_cyl=mtcars.sort_values("mpg")
#print(cars_cyl[cars_cyl.cyl==8].head(3))   #2

cars_mean=mtcars[mtcars.cyl==6].groupby("cyl")
#print(cars_mean.mpg.mean())   #3

cars_mass=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2.000) & (mtcars.wt<=2.200)].groupby("cyl")
#print(cars_mass.mpg.mean())   #4

#print('not automatic: ' + str(len(mtcars[mtcars.am==0])) + '\n'
#      + 'automatic: ' + str(len(mtcars[mtcars.am==1])))   #5

#print('automatic + hp>100: ' + str(len(mtcars[(mtcars.am==1) & (mtcars.hp>=100)])))   #6

cars_weight_kg=mtcars['wt']*0.45359237
#print(cars_weight_kg)  #7