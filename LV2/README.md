1. i 2. zadatak ove vježbe su zahtjevali filtriranje teksta pomoću re biblioteke regularnim izrazima.
Za rješavanje tih zadataka bilo je potrebno naučiti sintaksu kako odrediti znakove koji vode u sljedeće stanje.

3. zadatak je kreiranje nasumičnog broja muškaraca i žena te dodjeljivanje njihove visine prema normalnoj razdiobi.
Nakon toga trebalo je dobiveno prikazati na grafu.
Zadatak se rješava pomoću numpy normalne razdiobe i random generatora, a graf se stvara pomoću matplotlib.pyplot.

4. zadatak je simuliranje bacanja kockica i prikazivanje histiograma bacanja.
Zadatak se rješava sličnim funkcijama prošlog zadatka.

U 5. zadatku se treba prikazati ovisnost konjskih snaga o potrošnji auta na temelju danih podataka. Uz to, potrebno
je prikazati i informaciju o težini pojedinog auta.
Za rješenje zadatka potrebno je izdvojiti informacije iz csv datoteke i spremiti ih u određena polja.
Nakon toga potrebno je pomoću matplotlib.pyplot-a prikazati ovisnost dobivenih podataka.

6. zadatak zahtjeva povećavanje brightnessa slike.
Učitavanje matrice slike ostvari se preko matplotlib.image biblioteke. Dobivenu matricu potrebno je pomnožiti s 
brojem većim od 1, kako bi se RGB vrijednosti u matrici povećale. U slučaju da umnožak bude preko 1, koristi
se funkcija np.clip da se matrica može ponovno prikazati kao slika.