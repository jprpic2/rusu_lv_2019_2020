# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 17:41:38 2020

@author: Jakov-PC
"""

import csv
import numpy as np
import matplotlib.pyplot as plt


def pravac(x, theta):
    return theta[1]*x + theta[0]
        

carInfo=[]
with open(r"..\resources\mtcars.csv", newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in spamreader:
        carInfo.append([i.split('\n', 1)[0] for i in row])
        
horsePowerIndex=carInfo[0].index("\"hp\"")
milesPerGallonIndex=carInfo[0].index("\"mpg\"")
weightIndex=carInfo[0].index("\"wt\"")

carInfo=np.asarray(carInfo) 
graphInfo=np.empty((len(carInfo)-1,3))

graphInfo[:,0]=carInfo[1:, horsePowerIndex]
graphInfo[:,1]=carInfo[1:, milesPerGallonIndex]
graphInfo[:,2]=carInfo[1:, weightIndex]

graphInfo = graphInfo[np.argsort(graphInfo[:, 0])]

x=graphInfo[:,0]
y=graphInfo[:,1]
x=np.reshape(x,(32,1))
    
    
plt.figure(1)
plt.xlabel("Horsepower")
plt.ylabel("Miles per gallon")
 
X = np.ones((len(graphInfo),1))
X = np.append(X, x, axis=1)
theta_direct = np.linalg.inv(np.transpose(X) @ X) @ np.transpose(X) @ y

xp = np.array([x.min(), x.max()])
yp = np.array([pravac(xp[0],theta_direct), pravac(xp[1],theta_direct)])

plt.plot(xp,yp,'r')
plt.scatter(x, y, s=graphInfo[:,2]*50, c='c')
plt.show()

print(y.min(),y.max(),np.mean(y))



