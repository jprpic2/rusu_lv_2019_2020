# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 21:04:02 2020

@author: Jakov-PC
"""
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np 


img = mpimg.imread(r"C:\Users\Jakov-PC\Desktop\rusu_lv_2019_2020\LV2\resources\tiger.png")

plt.figure(1)
plt.imshow(img)

img *= 1.5
img = np.clip(img, 0, 1)

plt.figure(2)
plt.imshow(img)

