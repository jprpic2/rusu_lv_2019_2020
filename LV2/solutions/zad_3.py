# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 13:38:57 2020

@author: Jakov-PC
"""
def makeGraph(dataArray,graphColor):
    plt.hist(dataArray, color=graphColor, density=True, edgecolor='k')
    plt.axvline(dataArray.mean(), color='k', linestyle='dashed', linewidth=1.5)
    min_ylim, max_ylim = plt.ylim()
    plt.text(dataArray.mean()*1.005, max_ylim*0.9, 'Mean: {:.2f}'.format(dataArray.mean()))

import numpy as np 
from numpy.random import default_rng
import matplotlib.pyplot as plt
rng = default_rng()

genders = rng.integers(2, size=(1, 100))
maleHeight = np.random.normal(loc=180, scale=7, size=np.count_nonzero(genders == 1))
femaleHeight = np.random.normal(loc=167, scale=7, size=np.count_nonzero(genders == 0))

plt.subplot(1,2,1)
plt.title("Male height distribution")
makeGraph(maleHeight,'b')


plt.subplot(1,2,2)
plt.title("Female height distribution")
makeGraph(femaleHeight,'r')
