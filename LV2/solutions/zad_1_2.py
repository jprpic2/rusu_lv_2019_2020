import re

def removeAtSign(mailNames):
    names=[]
    for name in mailNames:
        names.append(name[0:name.find('@')])
    return names

fname = 'mbox-short.txt'  #e.g. www.py4inf.com/code/romeo.txt
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
    
lines=[]
for line in fhand:
    lines.append(line)
    
linesAsStrings = ''.join(str(e) for e in lines)
mails = re.findall(r'\b[a-zA-Z]+@[a-zA-z.]+\b',linesAsStrings)

mailsList = []

for mail in mails:
    if(mail not in mailsList):
        mailsList.append(mail)


mailsAsStrings = ' '.join(str(e) for e in mailsList)
mailNames= re.findall(r'\b\S+@',mailsAsStrings)

mailNames=removeAtSign(mailNames)
mailNamesString = ' '.join(str(e) for e in mailNames)

testingMails='auh aahaha hhahh haaah ahh hhha hehe hihi hoho heaho 07sas84 jprpic2 HAHA HohHo hAh'
zadatak_A = re.findall(r'\b\S*[aA]+\S*\b', testingMails)
zadatak_B = re.findall(r'\b[^aA ]*[aA][^aA ]*\b', testingMails)
zadatak_C = re.findall(r'\b[^aA ]+\b', testingMails)
zadatak_D = re.findall(r'\b\S*[0-9]+\S*', testingMails)
zadatak_E = re.findall(r'\b[a-z]+\b', testingMails)

print("Prvi dio mailova iz txt datoteke: ", mailNames)
print("Svi mailovi sa slovima a: ",zadatak_A)
print("Svi mailovi s jednim slovom a: ",zadatak_B)
print("Svi mailovi bez slova a: ",zadatak_C)
print("Svi mailovi sa zanemnkama: ",zadatak_D)
print("Svi mailovi s malim slovima: ",zadatak_E)